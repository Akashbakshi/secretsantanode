var express = require('express');
const mysql = require('mysql');
const fs = require('fs');
const passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const session = require('express-session');
const bodyParser = require('body-parser');
const moment = require('moment');
var dbInfo = JSON.parse(fs.readFileSync("db.json"));
var config = JSON.parse(fs.readFileSync("config.json"));
var app = express();
let m = moment();
const port = 3030;
var query_result = [];
var members = [];
var groupAdmin = [];
var memberGroups = [];
const bcrypt = require('bcrypt');
const appUrl = require('url');
const query_users = "	SELECT distinct concat(person.firstname,' ' ,person.lastname) as name from person";
const query_wishlist = "SELECT item_name, item_description,url,date_added,case purchased when 0 then 'No' else 'Yes' end purchased,price, CONCAT(person.firstname, ' ',person.lastname) as name FROM wishlist left outer join person on fk__owner = person.username;";
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.use(session({secret:"santa"}));
app.use(passport.initialize());
app.use(bodyParser.urlencoded({extended: true}));
app.use(passport.session()); // persistent login sessions


app.locals.user_menuoption=[
  "Home",
  "Wish List",
  "Groups"  
];

app.locals.user_links=[
  "/",
  "/WishList",
  "/Groups"
];


app.locals.regular_menuoption=[
  "Home",
  "News",
  "Join Now",
  "FAQ"  
];

app.locals.regular_links=[
  "/",
  "/news",
  "/registration",
  "/faq"
];

// Database connection
console.log('Attempting DB Connection!');
const connection = mysql.createConnection({
  
  host: dbInfo.hostname,
  user: dbInfo.username,
  password: dbInfo.password,
  database: dbInfo.database
});

connection.connect((err) => {
  if (err) 
    throw err;
  console.log('Database Connected!');
  setInterval(function () {
    connection.query('SELECT 1');
}, 5000);
   
});

/*
passport.use(new GoogleStrategy({
  clientID: config.GOOGLE_CLIENT_ID,
  clientSecret: config.GOOGLE_CLIENT_SECRET,
  callbackURL: "http://www.example.com/auth/google/callback"
},
function(accessToken, refreshToken, profile, done) {
     User.findOrCreate({ googleId: profile.id }, function (err, user) {
       return done(err, user);
     });
}
));
*/

app.set('view engine','pug');

app.get('/',function(req,res){
  
    connection.query(query_users,function(err,data,fields){
        if(err)
          throw err;

          var results = Object.values(JSON.parse(JSON.stringify(data)));
          members =[];
          results.forEach(function(b){
              members.push(b);
          });
    });

     connection.query(query_wishlist, function (err, data, fields) {
      if (err) 
        throw err;
        var resultArray = Object.values(JSON.parse(JSON.stringify(data)))
        query_result = [];
        resultArray.forEach(function(v){
              query_result.push(v);
          })
         
         if(req.session.user){
          req.session.errorMsg = "";
          res.render(__dirname+'/view/dashboard.pug',{items:query_result,members: members,user:req.session.user});
         }else{
          res.render(__dirname+"/view/index.pug");
         }
         
         
         
    });
   
   
});


app.get('/Groups',function(req,res){
  if(req.session.user){
    const adMemberQuery = "SELECT * FROM ss_group where fk__owner = '"+req.session.user+"';"
    groupAdmin = [];
    
    connection.query(adMemberQuery,function(err,results,fields){ 
        var resultArray = Object.values(JSON.parse(JSON.stringify(results)))
        resultArray.forEach(function(val){
          groupAdmin.push(val.group_name);
        })
        res.render(__dirname+'/view/groups.pug',{user:req.session.user,errors:req.session.errorMsg,adGroups:groupAdmin,memGroups:memberGroups});
    })
    
  }
  else
    res.redirect('/login');

});

app.get('/member_create_submit',function(req,res){
  
});


app.get('/group_settings',function(req,res){
    req.session.currentGroup = appUrl.parse(req.url,true).query.group;

    const adAdminQuery  = "SELECT * FROM ss_group where fk__owner = '"+req.session.user+"';"
    
    var adminMember;

    var adFirstName;
    var adLastName;

    connection.query(adAdminQuery ,function(err,results,fields){
      
      var resultArray = Object.values(JSON.parse(JSON.stringify(results)))

      resultArray.forEach(function(val){
        adminMember = val.fk__owner;
      })

      const adDetailsQuery = "SELECT * FROM person where username = '"+adminMember+"';"
      connection.query(adDetailsQuery,function(err,results,fields){
        var resultsInfo = Object.values(JSON.parse(JSON.stringify(results)));

        resultsInfo.forEach(function(val){
           adFirstName = val.firstname;
           adLastName = val.lastname;
        })
        console.log(adFirstName+" "+adLastName+" "+adminMember)
        if(req.session.user)
          res.render(__dirname+'/view/group_settings',{user:req.session.user,adMember:adminMember,adfname: adFirstName, adlname: adLastName, memGroups:memberGroups});
        else
          res.redirect('/login');
      })

    });

    

    

   
});

app.get('/WishList',function(req,res){
    

    if(req.session.user){
      req.session.errorMsg = "";
      var user_wishlist = "SELECT * FROM wishlist WHERE fk__owner = '"+req.session.user+"';"
      connection.query(user_wishlist, function (err, data, fields) {
        if (err) 
          throw err;
          var wishlistVals = Object.values(JSON.parse(JSON.stringify(data)));
          query_result = [];
          wishlistVals.forEach(function(v){
                query_result.push(v);
            })
    
            res.render(__dirname+'/view/wishlist.pug',{wishlist:query_result,user:req.session.user});
      });
    }
    else
      res.redirect('/login');
});

app.get('/wishlist_redirect',function(req,res){

  res.redirect('/WishList');
});

app.post('/wishlist_updated',function(req,res){
  var item_name = req.body.it_name;
  var item_descr = req.body.it_descr;
  var url = req.body.it_url;
  var price = req.body.it_price;

  var moment_date =  new moment().format("YYYY-MM-DD[T]HH:mm:ss");
  
  var add_wishlist = "INSERT INTO wishlist(item_name,item_description,url,date_added,price,fk__owner) VALUES('"+item_name+"', '"+item_descr+"', '"+url+"', '"+moment_date+"', '"+price+"', '"+req.session.user+"');";

  connection.query(add_wishlist,function(err,data,fields){
      if(err)
        throw err;
      
  });
  res.redirect('/wishlist_redirect');
});
app.get('/login',function(req,res){
    res.render(__dirname+'/view/login.pug',{message:""});
  
});

app.get('/logout',function(req,res){
    if(req.session.user){
      req.session.destroy();
      res.redirect('/');
    }
});
app.post('/registration-submit',function(req,res){
    var username = req.body.username;
    var password = bcrypt.hashSync(req.body.password,10);
    var email = req.body.email;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;

    var query = "SELECT * from person where username='"+username+"';"
    connection.query(query,function(error,data,fields){

    if(data.length>0){
      res.render(__dirname+'/view/registration.pug',{message: "Username Exists"});
    }else{
      var insert_query = "INSERT INTO person(username,email,password,firstname,lastname) values('"+username+"', '"+email+"', '"+password+"', '"+firstname+"', '"+lastname+"');";
    
      connection.query(insert_query,function(err,data,fields){
          if(err)
            throw err;
          else{
            res.redirect('/');
          }
      });
    }

  });

});

app.get('/registration',function(req,res){
  res.render(__dirname+'/view/registration.pug',{message:""});
});

app.post('/group_create_submit',function(req,res){

  const groupNameCheck = "select * from ss_group where group_name = '"+req.body.groupName+"';";

  const makeGroup = "INSERT INTO ss_group(group_name,fk__owner) VALUES("+"'"+req.body.groupName+"', '"+req.session.user+"');";
  connection.query(groupNameCheck,function(err,results,fields){
    if(results.length == 0){
      connection.query(makeGroup);
      req.session.errorMsg = "";
      res.redirect('/Groups');
    }else{
      req.session.errorMsg="Group Already Exists"
      res.redirect('/Groups');
    }
  });
  
});


app.post('/login_submit',function(req,res){
  var username = req.body.username;
  var pass_match;
  var query_pass = [];
  connection.query("select password from person where username='"+username+"';",function(err,results,fields){
    if(results.length>0){
      var resultArray = Object.values(JSON.parse(JSON.stringify(results)));

      resultArray.forEach(function(v){
        query_pass.push(v.password);
      });
    
      if(bcrypt.compareSync(req.body.pass, query_pass[0])) {
        var login_query = "select * from person where username='"+username+"' and password='"+query_pass[0]+"';";
        
        connection.query(login_query,function (err, data, fields) {
            if(data.length>0){
              
            req.session.user = username;
            res.redirect('/');
            }else{
              res.render(__dirname+'/view/login.pug',{message:"Invalid Username or Password!"});
            }
        }); 
        
      }
      else{
        res.render(__dirname+'/view/login.pug',{message:"Invalid Username or Password!"})
      }
  }else{
    res.render(__dirname+'/view/login.pug',{message:"Invalid Username or Password!"})
  }
  
  });
  
});
app.listen(port,()=>console.log("Site is live on port: "+port))