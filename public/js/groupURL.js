

function setURLParam(link){
    
    // this function will set the group name in the url so when we load the new page we can get the group name and have dynamic group pages
    var groupName = link.getElementsByTagName("a")[0].innerHTML;
    var newURL = link.getElementsByTagName("a")[0].href; // get the href of the page we're heading to 
    var url = new URL(newURL); // create a new href with the location we just got
 
    url.searchParams.append('group',groupName); // add group with the group name 
    window.location.assign(url); // assign the new URL we just created to this

}

function getGroupFromURL(){
    //this function will do the opposite of the setURLParams by getting the group name from the param we set

    var groupName = window.location.search.split('group=')[1]; // get the group name from search params


    document.getElementById('groupAdminLB').innerHTML=
    console.log(groupName);

    return groupName;
}

function setGroupTitle(){
    var titleElement = document.getElementById('groupTitle');
    titleElement.innerHTML =  "<center>"+getGroupFromURL()+" Home</center>";
}

function setHeaderNames(){
    var groupName = getGroupFromURL();
    setGroupTitle();
    document.getElementById('groupAdminLB').innerHTML= groupName+ "'s Admin";
    document.getElementById('groupMemberLB').innerHTML= groupName + "'s Members";
    document.getElementById('groupWishlist').innerHTML= groupName + "'s Wishlist";

}